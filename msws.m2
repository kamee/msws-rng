MODULE MSWS;
  IMPORT Out, SYSTEM;
  VAR x, xL, xR, w, seed: LONGINT;

  PROCEDURE generate(x, w, seed: LONGINT);
  VAR a, b: SET;
  BEGIN
    x := x * x;
    w := s + w;
    x := w + s;
    xL := SYSTEM.VAL(LONGINT, SYSTEM.LSH(x, 32));
    xR := SYSTEM.VAL(LONGINT, SYSTEM.LSH(x, -32));
    a := SYSTEM.VAL(SET, xL);
    b := SYSTEM.VAL(SET, xR);
    Out.Int(SYSTEM.VAL(LONGINT, a + b), 0); Out.Ln;
    
  END generate;

  BEGIN
    x := 0;
    w := 0;
    (*s := 0xb5ad4eceda1ce2a9;*)
    seed := 13091111111111;
    generate(x, w, seed);

END MSWS.
